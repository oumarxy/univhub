<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libelle')->nullable();
            $table->string('description')->nullable();
            $table->string('animateur')->nullable();
            $table->unsignedInteger('teacher_id')->nullable();
            $table->unsignedInteger('section_id')->nullable();
            $table->unsignedInteger('salle_id');
            $table->unsignedInteger('activite_id');
            $table->unsignedInteger('subject_id')->nullable();
            $table->unsignedInteger('class_id')->nullable();
            $table->dateTime('date_debut');
            $table->dateTime('date_fin');
            $table->enum('status', [0, 1])->default(1);
            $table->softDeletes();
            $table->userstamps();
            $table->timestamps();


            $table->foreign('teacher_id')->references('id')->on('employees');
            $table->foreign('salle_id')->references('id')->on('salles');
            $table->foreign('activite_id')->references('id')->on('activites');
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->foreign('class_id')->references('id')->on('i_classes');
            $table->foreign('section_id')->references('id')->on('sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
