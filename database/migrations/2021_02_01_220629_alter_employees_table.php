<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
          
            $table->string('nationalite',100)->nullable();
            $table->string('fonction_administrative',200)->nullable();
            $table->string('situation_matrimoniale',100)->nullable();
            $table->integer('nombre_enfant')->nullable();
            $table->string('specialite',200)->nullable();
            $table->string('ufr',200)->nullable();
            $table->string('type_enseignant',100)->nullable();
            $table->string('permissionnaire',200)->nullable();
            $table->string('permission_start',10)->nullable();
            $table->string('permission_end',10)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
