<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\UserRole;
use App\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        echo PHP_EOL , 'cleaning old data....';

        //DB::statement("SET foreign_key_checks=0");
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('user_roles')->delete();
        DB::table('permissions')->delete();
       // User::delete();
       // Role::delete();
       // UserRole::delete();
        //Permission::delete();
        DB::table('roles_permissions')->delete();
        DB::table('users_permissions')->delete();

        //DB::statement("SET foreign_key_checks=1");

        $this->call(RolesTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);

    }
}
