<?php

namespace App\Http\Controllers\Backend;

use App\Http\Helpers\AppHelper;
use App\User;
use App\Salle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $salles = Salle::all();

        return view('backend.salle.list', compact('salles'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salle = null;
        return view('backend.salle.add', compact('salle'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate form
        $messages = [
            'photo.max' => 'The :attribute size must be under 200kb.',
            'photo.dimensions' => 'The :attribute dimensions min 150 X 150.',
        ];
        $this->validate(
            $request, [
                'categorie' => 'required',
                'code_salle' => 'required',
                'libelle' => 'required|min:2|max:255',
                'photo' => 'mimes:jpeg,jpg,png|max:200|dimensions:min_width=150,min_height=150',
                'description' => 'max:255',
            ]
        );

     //  if($request->hasFile('photo')) {
     //      $storagepath = $request->file('photo')->store('public/salle');
     //      $fileName = basename($storagepath);
     //      $data['photo'] = $fileName;
     //  }
     //  else{
     //      $data['photo'] = $request->get('oldPhoto','');
     //  }
        
        $data['categorie'] = $request->get('categorie');
        $data['code_salle'] = $request->get('code_salle');
        $data['libelle'] = $request->get('libelle');
        $data['description'] = $request->get('description');
        $data['longitude'] = $request->get('longitude');
        $data['latitude'] = $request->get('latitude');

        DB::beginTransaction();
        try {
            //now create salle
            $salle = Salle::create(
                [
                    'categorie' => $data['categorie'],
                    'code_salle' => $data['code_salle'],
                    'libelle' => $data['libelle'],
                    'description' => $data['description'],
                    'longitude' => $data['longitude'],
                    'latitude' => $data['latitude'],
                  //  'photo' => $data['photo'],
                ]
            );
            // now save salle
            //Salle::create($salle);
            DB::commit();
            return redirect()->route('salle.index')->with('success', 'Salle ajoutée!');
        }
        catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
//            return $message;
            return redirect()->route('salle.create')->with("error",$message);
        }
        return redirect()->route('salle.create');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $salle = Salle::where('id', $id)->first();
        if(!$salle){
            abort(404);
        }

        return view('backend.salle.view', compact('salle'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salle = Salle::where('id', $id)->first();

        if(!$salle){
            abort(404);
        }
       
        return view('backend.salle.add', compact('salle'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $salle = Salle::where('id', $id)->first();

        if(!$salle){
            abort(404);
        }
        //validate form
        $messages = [
            'photo.max' => 'The :attribute size must be under 200kb.',
            'photo.dimensions' => 'The :attribute dimensions min 150 X 150.',
        ];
        $this->validate(
            $request, [
                'categorie' => 'required',
                'code_salle' => 'required',
                'libelle' => 'required|min:2|max:255',
                'photo' => 'mimes:jpeg,jpg,png|max:200|dimensions:min_width=150,min_height=150',
                'description' => 'max:255'
            ]
        );

        if($request->hasFile('photo')) {
            $storagepath = $request->file('photo')->store('public/salle');
            $fileName = basename($storagepath);
            $data['photo'] = $fileName;

            //if file change then delete old one
            $oldFile = $request->get('oldPhoto','');
            if( $oldFile != ''){
                $file_path = "public/salle/".$oldFile;
                Storage::delete($file_path);
            }
        }
        else{
            $data['photo'] = $request->get('oldPhoto','');
        }

        
        $data['categorie'] = $request->get('categorie');
        $data['code_salle'] = $request->get('code_salle');
        $data['libelle'] = $request->get('libelle');
        $data['description'] = $request->get('description');
        $data['longitude'] = $request->get('longitude');
        $data['latitude'] = $request->get('latitude');


        $salle->fill($data);
        $salle->save();

        return redirect()->route('salle.index')->with('success', 'Salle mofifiée avec succès !');


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salle = Salle::where('id', $id)->first();

        if(!$salle){
            abort(404);
        }
        $message = "Erreur lors du traitement!";
        DB::beginTransaction();
        try {

            $salle->delete();
            DB::commit();
            return redirect()->route('salle.index')->with('success', 'Salle supprimée avec succès.');
        }
        catch(\Exception $e){
            DB::rollback();
            $message = str_replace(array("\r", "\n","'","`"), ' ', $e->getMessage());
        }
        return redirect()->route('salle.index')->with('error', $message);

    }

    /**
     * status change
     * @return mixed
     */
    public function changeStatus(Request $request, $id=0)
    {

        $salle =  Salle::findOrFail($id);
        if(!$salle){
            return [
                'success' => false,
                'message' => 'Enregistrement non trouvé!'
            ];
        }

        $salle->status = (string)$request->get('status');

        $salle->save();

        return [
            'success' => true,
            'message' => 'Status modifié.'
        ];

    }
}
