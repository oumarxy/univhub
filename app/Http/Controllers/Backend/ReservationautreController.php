<?php

namespace App\Http\Controllers\Backend;

use App\Http\Helpers\AppHelper;
use App\User;
use App\Salle;
use App\IClass;
use App\Activite;
use App\Subject;
use App\Section;
use App\Reservation;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReservationautreController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $cours_id=1;

        $reservations = Reservation::where('activite_id','!=',$cours_id)->get();
       
        return view('backend.reservationautre.list', compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $activite_id = null;
        $salle_id = null;
        $reservation = null;

        /*  $classes = IClass::where('status', AppHelper::ACTIVE)
          ->orderBy('order', 'asc')
          ->pluck('name', 'id');
         */
        $activites = Activite::where('status', AppHelper::ACTIVE)
                ->pluck('libelle', 'id');
        $salles = Salle::where('status', AppHelper::ACTIVE)
                ->pluck('libelle', 'id');
        /* $subjects = Subject::where('status', AppHelper::ACTIVE)
          ->pluck('name', 'id'); */
        /*   $teachers = Employee::where('role_id', AppHelper::EMP_TEACHER)
          ->where('status', AppHelper::ACTIVE)
          ->pluck('name', 'id'); */

        return view('backend.reservationautre.add',
                compact('reservation', 'salles', 'activites', 'activite_id', 'salle_id'));
    }

    public function subjectIndex(Request $request) {
        // check for ajax request here
        if ($request->ajax()) {
            $class_id = $request->query->get('class', 0);
            /*  $teacherId = 0;
              if(session('user_role_id',0) == AppHelper::USER_TEACHER){
              $teacherId = auth()->user()->teacher->id;
              } */
            $subjects = Subject::select('id', 'name as text')
                    ->where('class_id', $class_id)
                    /*  ->sType($subjectType)
                      ->when($teacherId, function ($query) use($teacherId){
                      $query->where('teacher_id', $teacherId);
                      }) */
                    ->where('status', AppHelper::ACTIVE)
                    ->orderBy('name', 'asc')
                    ->get();
            return $subjects;
        }

        abort(404);
        return;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $data['libelle'] = $request->get('libelle');
        $data['description'] = $request->get('description');
        $data['animateur'] = $request->get('animateur');
        $data['activite_id'] = $request->get('activite_id');
        $data['salle_id'] = $request->get('salle_id');
        $data['date_debut'] = $request->get('date_debut');
        $data['date_fin'] = $request->get('date_fin');


        DB::beginTransaction();
        try {
            //now create reservation

             $date_debut =  $data['date_debut']; // Carbon::parse($data['date_debut'])->timezone('Africa/Abidjan')->format('Y-m-d h:i:s');
              //die($date_debut);
                $date_fin =  $data['date_fin']; //Carbon::parse($data['date_fin'])->timezone('Africa/Abidjan')->format('Y-m-d h:i:s');


            //$todayWithMarge = Carbon::now()->addMinutes(self::MARGEOCCUPATIONSALLE);
            $reservationVerifDebut = Reservation::where([
                        ['reservations.salle_id', '=', $data['salle_id']],
                        ['reservations.status', '=', AppHelper::ACTIVE],
                        ['reservations.date_debut', '<=', $date_debut],
                        ['reservations.date_fin', '>=', $date_debut]
                    ])->first();
            $reservationVerifFin = Reservation::where([
                        ['reservations.salle_id', '=', $data['salle_id']],
                        ['reservations.status', '=', AppHelper::ACTIVE],
                        ['reservations.date_debut', '<=', $date_fin],
                        ['reservations.date_fin', '>=', $date_fin]
                    ])->first();
            if ($reservationVerifDebut != null || $reservationVerifFin != null) {
                $message = "Imbrication de date de reservation :";
                return redirect()->route('reservationautre.create')->with("error", $message);
            }

            $reservation = Reservation::create(
                            [
                                'libelle' => $data['libelle'],
                                'description' => $data['description'],
                                'animateur' => $data['animateur'],
                                'salle_id' => $data['salle_id'],
                                'activite_id' => $data['activite_id'],
                                'date_debut' => $date_debut,
                                'date_fin' => $date_fin,
                            ]
            );
            // now save reservation
            //Reservation::create($reservation);
            DB::commit();
            return redirect()->route('reservationautre.index')->with('success', 'Reservation ajoutée!');
        } catch (\Exception $e) {
            DB::rollback();
            $message = str_replace(array("\r", "\n", "'", "`"), ' ', $e->getMessage());
//            return $message;
            return redirect()->route('reservationautre.create')->with("error", $message);
        }
        return redirect()->route('reservationautre.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        $reservation = Reservation::where('id', $id)->first();
        if (!$reservation) {
            abort(404);
        }

        return view('backend.reservationautre.view', compact('reservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        /* $regiInfo = Registration::find($id);
          if(!$regiInfo){
          abort(404);
          } */
        $reservation = Reservation::where('id', $id)->first();

        if (!$reservation) {
            abort(404);
        }



        $activites = Activite::where('status', AppHelper::ACTIVE)
                ->pluck('libelle', 'id');
        $salles = Salle::where('status', AppHelper::ACTIVE)
                ->pluck('libelle', 'id');


        $salle_id = $reservation->salle_id;
        $activite_id = $reservation->activite_id;

        return view('backend.reservationautre.add',
                compact('reservation', 'salles', 'activites', 'salle_id', 'activite_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $reservation = Reservation::where('id', $id)->first();

        if (!$reservation) {
            abort(404);
        }
        //validate form
        //     $messages = [
        //         'photo.max' => 'The :attribute size must be under 200kb.',
        //         'photo.dimensions' => 'The :attribute dimensions min 150 X 150.',
        //     ];
        //dd($request->get('date_debut'));
        $date_debut = $request->get('date_debut'); // Carbon::parse($request->get('date_debut'))->timezone('Africa/Abidjan')->format('Y-m-d h:i:s');

        $date_fin = $request->get('date_fin'); //Carbon::parse($request->get('date_fin'))->timezone('Africa/Abidjan')->format('Y-m-d h:i:s');

        $reservationVerifDebut = Reservation::where([
                    ['reservations.salle_id', '=', $reservation->salle_id],
                    ['reservations.status', '=', AppHelper::ACTIVE],
                    ['reservations.date_debut', '<=', $date_debut],
                    ['reservations.date_fin', '>=', $date_debut]
                ])->first();
        $reservationVerifFin = Reservation::where([
                    ['reservations.salle_id', '=', $reservation->salle_id],
                    ['reservations.status', '=', AppHelper::ACTIVE],
                    ['reservations.date_debut', '<=', $date_fin],
                    ['reservations.date_fin', '>=', $date_fin]
                ])->first();
        if ($reservationVerifDebut != null || $reservationVerifFin != null) {
            $message = "Imbrication de date de reservation :";
            return redirect()->route('reservationautre.edit', $id)->with("error", $message);
        }

        $data['libelle'] = $request->get('libelle');
        $data['description'] = $request->get('description');
        $data['animateur'] = $request->get('animateur');
        $data['salle_id'] = $request->get('salle_id');
        $data['activite_id'] = $request->get('activite_id');
        $data['date_debut'] = $date_debut;
        $data['date_fin'] = $date_fin;

        $reservation->fill($data);
        $reservation->save();

        return redirect()->route('reservationautre.index')->with('success', 'Reservation mofifiée avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $reservation = Reservation::where('id', $id)->first();

        if (!$reservation) {
            abort(404);
        }
        $message = "Erreur lors du traitement!";
        DB::beginTransaction();
        try {

            $reservation->delete();
            DB::commit();
            return redirect()->route('reservationautre.index')->with('success', 'Reservation supprimée avec succès.');
        } catch (\Exception $e) {
            DB::rollback();
            $message = str_replace(array("\r", "\n", "'", "`"), ' ', $e->getMessage());
        }
        return redirect()->route('reservationautre.index')->with('error', $message);
    }

    /**
     * status change
     * @return mixed
     */
    public function changeStatus(Request $request, $id = 0) {

        $reservation = Reservation::findOrFail($id);
        if (!$reservation) {
            return [
                'success' => false,
                'message' => 'Enregistrement non trouvé!'
            ];
        }
        $reservation->status = (string) $request->get('status');

        $reservation->save();

        return [
            'success' => true,
            'message' => 'Status modifié.'
        ];
    }

}
