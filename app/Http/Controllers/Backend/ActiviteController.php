<?php

namespace App\Http\Controllers\Backend;

use App\Http\Helpers\AppHelper;
use App\User;
use App\Activite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ActiviteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $activites = Activite::all();

        return view('backend.activite.list', compact('activites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $activite = null;
        return view('backend.activite.add', compact('activite'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate form
        $messages = [];
        $this->validate(
                $request, [
            'libelle' => 'required|min:2|max:255',
                ]
        );

        //  if($request->hasFile('photo')) {
        //      $storagepath = $request->file('photo')->store('public/activite');
        //      $fileName = basename($storagepath);
        //      $data['photo'] = $fileName;
        //  }
        //  else{
        //      $data['photo'] = $request->get('oldPhoto','');
        //  }

        $data['libelle'] = $request->get('libelle');

        DB::beginTransaction();
        try {
            //now create activite
            $activite = Activite::create(
                            [
                                'libelle' => $data['libelle'],
                            ]
            );
            // now save activite
            //Activite::create($activite);
            DB::commit();
            return redirect()->route('activite.index')->with('success', 'Activite ajoutée!');
        } catch (\Exception $e) {
            DB::rollback();
            $message = str_replace(array("\r", "\n", "'", "`"), ' ', $e->getMessage());
//            return $message;
            return redirect()->route('activite.create')->with("error", $message);
        }
        return redirect()->route('activite.create');
    }

    function validateLatLong($lat, $long) {
        return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $lat . ',' . $long);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        $activite = Activite::where('id', $id)->first();
        if (!$activite) {
            abort(404);
        }

        return view('backend.activite.view', compact('activite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $activite = Activite::where('id', $id)->first();

        if (!$activite) {
            abort(404);
        }

        return view('backend.activite.add', compact('activite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $activite = Activite::where('id', $id)->first();

        if (!$activite) {
            abort(404);
        }
        //validate form
        $messages = [ ];
        $this->validate(
                $request, [
           'libelle' => 'required|min:2|max:255' ]
        );

        $data['libelle'] = $request->get('libelle');

        $activite->fill($data);
        $activite->save();

        return redirect()->route('activite.index')->with('success', 'Activite mofifiée avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $activite = Activite::where('id', $id)->first();

        if (!$activite) {
            abort(404);
        }
        $message = "Erreur lors du traitement!";
        DB::beginTransaction();
        try {

            $activite->delete();
            DB::commit();
            return redirect()->route('activite.index')->with('success', 'Activite supprimée avec succès.');
        } catch (\Exception $e) {
            DB::rollback();
            $message = str_replace(array("\r", "\n", "'", "`"), ' ', $e->getMessage());
        }
        return redirect()->route('activite.index')->with('error', $message);
    }

    /**
     * status change
     * @return mixed
     */
    public function changeStatus(Request $request, $id = 0) {

        $activite = Activite::findOrFail($id);
        if (!$activite) {
            return [
                'success' => false,
                'message' => 'Enregistrement non trouvé!'
            ];
        }

        $activite->status = (string) $request->get('status');

        $activite->save();

        return [
            'success' => true,
            'message' => 'Status modifié.'
        ];
    }

}
