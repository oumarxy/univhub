<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hrshadhin\Userstamps\UserstampsTrait;

class Salle extends Model
{
    use SoftDeletes;
    use UserstampsTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'categorie',
        'code_salle',
        'libelle',
        'description',
        'longitude',
        'latitude',
        'status',
    ];

/*
    public function section()
    {
        return $this->hasMany('App\Section', 'class_id');
    }

    public function student()
    {
        return $this->hasMany('App\Registration', 'class_id');
    }

    public function attendance()
    {
        return $this->hasMany('App\StudentAttendance', 'class_id');
    }
 * *
 */
    
}
