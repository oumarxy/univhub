<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hrshadhin\Userstamps\UserstampsTrait;

class Reservation extends Model
{
    use SoftDeletes;
    use UserstampsTrait;

//protected $dateFormat = 'Y-m-d H:i:s0';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'libelle',
        'description',
        'animateur',
        'teacher_id',
        'salle_id',      
        'class_id',
        'activite_id',
        'section_id',
        'subject_id',
        'status',
        'date_debut',
        'date_fin',
    ];


    public function teacher()
    {
        return $this->belongsTo('App\Employee', 'teacher_id');
    }
    public function section()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }
    public function salle()
    {
        return $this->belongsTo('App\Salle', 'salle_id');
    }
    public function class()
    {
        return $this->belongsTo('App\IClass', 'class_id');
    }
    public function activite()
    {
        return $this->belongsTo('App\Activite', 'activite_id');
    }
    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    
}
