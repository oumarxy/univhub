
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- sidebar menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="{{ URL::route('user.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Tableau de bord</span>
                </a>
            </li>
            @canany(['salle.index', 'activite.index'])
            <li class="treeview">
                <a href="#">
                    <i class="fa icon-academicmain"></i>
                    <span>Infrastruction</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @notrole('Student')
                    @can('salle.index')
                    <li>
                        <a href="{{ URL::route('salle.index') }}">
                            <i class="fa fa-home"></i> <span>Salles</span>
                        </a>
                    </li>
                    @endcan
                    @can('activite.index')
                    <li>
                        <a href="{{ URL::route('activite.index') }}">
                            <i class="fa fa-home"></i> <span>Activite</span>
                        </a>
                    </li>
                    @endcan
                    
                    @endnotrole
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-clock-o"></i><span>Routine</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </li>
            @endcanany

            <li class="treeview">
                <a href="#">
                    <i class="fa icon-academicmain"></i>
                    <span>Programmation</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  
                   
                    @can('reservation.index')
                    <li>
                        <a href="{{ URL::route('reservation.index') }}">
                            <i class="fa fa-home"></i> <span>Cours</span>
                        </a>
                    </li>
                    @endcan
                     @can('reservation.index')
                    <li>
                        <a href="{{ URL::route('reservationautre.index') }}">
                            <i class="fa fa-home"></i> <span>Autres</span>
                        </a>
                    </li>
                    @endcan
                    
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-clock-o"></i><span>Routine</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </li>

            @can('teacher.index')
            <li>
                <a href="{{ URL::route('teacher.index') }}">
                    <i class="fa icon-teacher"></i> <span>Enseignants</span>
                </a>
            </li>
            @endcan

            @can('student.index')
            <li>
                <a href="{{ URL::route('student.index') }}">
                    <i class="fa icon-student"></i> <span>Etudiants</span>
                </a>
            </li>
            @endcan


            @canany(['student_attendance.index', 'employee_attendance.index'])
            <!--<li class="treeview">
                <a href="#">
                    <i class="fa icon-attendance"></i>
                    <span>Présence</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('student_attendance.index') }}">
                            <i class="fa icon-student"></i> <span>Présence des étudiants</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('employee_attendance.index') }}">
                            <i class="fa icon-member"></i> <span>Présence des employés</span>
                        </a>
                    </li>
                </ul>
            </li>-->
            @endcanany


            <li class="treeview">
                <a href="#">
                    <i class="fa icon-academicmain"></i>
                    <span>Scolarité</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @notrole('Student')
                    @can('academic.class')
                    <li>
                        <a href="{{ URL::route('academic.class') }}">
                            <i class="fa fa-sitemap"></i> <span>Niveau</span>
                        </a>
                    </li>
                    @endcan
                    @can('academic.section')
                    <li>
                        <a href="{{ URL::route('academic.section') }}">
                            <i class="fa fa-cubes"></i> <span>Groupe</span>
                        </a>
                    </li>
                    @endcan
                    @endnotrole

                    @can('academic.subject')
                    <li>
                        <a href="{{ URL::route('academic.subject') }}">
                            <i class="fa icon-subject"></i> <span>Enseignement(ECUE)</span>
                        </a>
                    </li>
                    @endcan

                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-clock-o"></i><span>Routine</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                </ul>
            </li>

            @notrole('Student')
            <!--  
              <li class="treeview">
                  <a href="#">
                      <i class="fa icon-exam"></i>
                      <span>Examens</span>
                      <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                      @can('exam.index')
                      <li>
                          <a href="{{ URL::route('exam.index') }}">
                              <i class="fa icon-exam"></i> <span>Examen</span>
                          </a>
                      </li>
                      @endcan
                      @can('exam.grade.index')
                      <li>
                          <a href="{{ URL::route('exam.grade.index') }}">
                              <i class="fa fa-bar-chart"></i> <span>Grade</span>
                          </a>
                      </li>
                      @endcan
                      @can('exam.rule.index')
                      <li>
                          <a href="{{ URL::route('exam.rule.index') }}">
                              <i class="fa fa-cog"></i> <span>Roles</span>
                          </a>
                      </li>
                      @endcan
                  </ul>
              </li>
            -->

            <!--  
              <li class="treeview">
                  <a href="#">
                      <i class="fa icon-markmain"></i>
                      <span>Marques & Resultats</span>
                      <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                      @can('marks.index')
                      <li>
                          <a href="{{ URL::route('marks.index') }}">
                              <i class="fa icon-markmain"></i> <span>Marques</span>
                          </a>
                      </li>
                      @endcan
                      @can('result.index')
                      <li>
                          <a href="{{ URL::route('result.index') }}">
                              <i class="fa icon-markpercentage"></i> <span>Resultat</span>
                          </a>
                      </li>
                      @endcan
                  </ul>
              </li>
            -->
            @endnotrole
            @notrole('Student')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Ressources Humaines</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('hrm.employee.index')
                    <li>
                        <a href="{{ URL::route('hrm.employee.index') }}">
                            <i class="fa icon-member"></i> <span>Employés</span>
                        </a>
                    </li>
                    @endcan
                    @can('hrm.leave.index')
                    <li>
                        <a href="{{ URL::route('hrm.leave.index') }}">
                            <i class="fa fa-bed"></i> <span>Laisser</span>
                        </a>
                    </li>
                    @endcan

                    @can('hrm.work_outside.index')
                    <li>
                        <a href="{{ URL::route('hrm.work_outside.index') }}">
                            <i class="glyphicon glyphicon-log-out"></i> <span>Travailler dehors</span>
                        </a>
                    </li>
                    @endcan
                    @can('hrm.policy')
                    <li>
                        <a href="{{ URL::route('hrm.policy') }}">
                            <i class="fa fa-cogs"></i> <span>Politique</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endnotrole
            @role('Admin')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-secret"></i>
                    <span>Administrateur</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('administrator.academic_year') }}">
                            <i class="fa fa-calendar-plus-o"></i> <span>Année Academique</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('administrator.template.mailsms.index') }}">
                            <i class="fa icon-mailandsms"></i> <span>Modèle deMail/SMS </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('administrator.template.idcard.index') }}">
                            <i class="fa fa-id-card"></i> <span>Modèle de carte d'identité</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{URL::route('administrator.user_index')}}">
                            <i class="fa fa-user-md"></i> <span>Administrateur système</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('administrator.user_password_reset')}}">
                            <i class="fa fa-eye-slash"></i> <span>Réinitialiser le mot de passe</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{URL::route('user.role_index')}}">
                            <i class="fa fa-users"></i> <span>Role</span>
                        </a>
                    </li>

                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-download"></i> <span>Sauvegarde</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--<i class="fa fa-upload"></i> <span>Restorer</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                </ul>
            </li>
            @endrole
            @can('user.index')
            <li>
                <a href="{{ URL::route('user.index') }}">
                    <i class="fa fa-users"></i> <span>Utilisateurs</span>
                </a>
            </li>
            @endcan


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-pdf-o"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#">
                            <i class="fa icon-studentreport"></i>
                            <span>Etudiant</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            {{--              @can('report.student_monthly_attendance')--}}
                            <li>
                                <a href="{{ URL::route('report.student_monthly_attendance') }}">
                                    <i class="fa icon-attendancereport"></i> <span>Présence mensuelle</span>
                                </a>
                            </li>
                            {{--@endcan--}}
                            <li>
                                <a href="{{route('report.student_list')}}">
                                    <i class="fa icon-student"></i> <span>Liste étudiants</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>HRM</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ URL::route('report.employee_monthly_attendance') }}"><i class="fa icon-attendancereport"></i> <span>Présence mensuelle</span></a>
                            </li>
                            <li>
                                <a href="{{route('report.employee_list')}}"><i class="fa icon-teacher"></i> <span>Liste des employés</span></a>
                            </li>

                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa icon-mark2"></i>
                            <span>Marques et résultat</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{route('report.marksheet_pub')}}"><i class="fa fa-file-pdf-o"></i><span>Feuille de pointage public</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

            @role('Admin')
            {{--
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ URL::route('settings.institute') }}">
                            <i class="fa fa-building"></i> <span>Institut</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('settings.academic_calendar.index') }}">
                            <i class="fa fa-calendar"></i> <span>Calendrier académique</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('settings.sms_gateway.index') }}">
                            <i class="fa fa-external-link"></i> <span>Passerelles SMS</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::route('settings.report') }}">
                            <i class="fa fa-file-pdf-o"></i> <span>rapport</span>
                        </a>
                    </li>
                </ul>
            </li>
            --}}
            @endrole
            <!-- Frontend Website links and settings -->
            @if($frontend_website)
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-globe"></i>
                    <span>Site</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('site.dashboard')
                    <li>
                        <a href="{{ URL::route('site.dashboard') }}">
                            <i class="fa fa-dashboard"></i> <span>Tableau de bord</span>
                        </a>
                    </li>
                    @endcan
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-home"></i>
                            <span>Acceuil</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            @can('site.index')
                            <li><a href="{{URL::route('slider.index')}}"><i class="fa fa-picture-o text-aqua"></i> Sliders</a></li>
                            @endcan
                            @can('site.about_content')
                            <li><a href="{{URL::route('site.about_content')}}"><i class="fa fa-info text-aqua"></i> À propos de nous</a></li>
                            @endcan
                            @can('site.service')
                            <li><a href="{{ URL::route('site.service') }}"><i class="fa fa-file-text text-aqua"></i> Nos Services</a></li>
                            @endcan
                            @can('site.statistic')
                            <li><a href="{{ URL::route('site.statistic') }}"><i class="fa fa-bars"></i> Statistique</a></li>
                            @endcan
                            @can('site.testimonial')
                            <li><a href="{{ URL::route('site.testimonial') }}"><i class="fa fa-comments"></i> Testimonials</a></li>
                            @endcan
                            @can('site.subscribe')
                            <li><a href="{{ URL::route('site.subscribe') }}"><i class="fa fa-users"></i> Les abonnés</a></li>
                            @endcan
                        </ul>
                    </li>
                    @can('class_profile.index')
                    <li>
                        <a href="{{ URL::route('class_profile.index') }}">
                            <i class="fa fa-building"></i>
                            <span>Classe</span>
                        </a>
                    </li>
                    @endcan
                    @can('teacher_profile.index')
                    <li>
                        <a href="{{ URL::route('teacher_profile.index') }}">
                            <i class="fa icon-teacher"></i>
                            <span>Enseignants</span>
                        </a>
                    </li>
                    @endcan
                    @can('event.index')
                    <li>
                        <a href="{{ URL::route('event.index') }}">
                            <i class="fa fa-bullhorn"></i>
                            <span>Events</span>
                        </a>
                    </li>
                    @endcan
                    @can('site.gallery')
                    <li>
                        <a href="{{ URL::route('site.gallery') }}">
                            <i class="fa fa-camera"></i>
                            <span>Gallerie</span>
                        </a>
                    </li>
                    @endcan
                    @can('site.contact_us')
                    <li>
                        <a href="{{ URL::route('site.contact_us') }}">
                            <i class="fa fa-map-marker"></i>
                            <span>Contactez-nous</span>
                        </a>
                    </li>
                    @endcan
                    @can('site.faq')
                    <li>
                        <a href="{{ URL::route('site.faq') }}">
                            <i class="fa fa-question-circle"></i>
                            <span>FAQ</span>
                        </a>

                    </li>
                    @endcan
                    @can('site.timeline')
                    <li>
                        <a href="{{ URL::route('site.timeline') }}"><i class="fa fa-clock-o"></i>
                            <span>Timeline</span>
                        </a>
                    </li>
                    @endcan
                    @can('site.settings')
                    <li>
                        <a href="{{ URL::route('site.settings') }}"><i class="fa fa-cogs"></i>
                            <span>Settings</span>
                        </a>
                    </li>
                    @endcan
                    @can('site.analytics')
                    <li>
                        <a href="{{ URL::route('site.analytics') }}"><i class="fa fa-line-chart"></i>
                            <span>Analytique</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
