<footer class="main-footer">
    <div class="pull-right">
        <!-- Don't remove below text. It's violate the license. -->
        <strong>UJLoG-Hub V1.0</strong>
    </div>
    <strong>Copyright &copy; {{date('Y')}}</strong>
</footer>