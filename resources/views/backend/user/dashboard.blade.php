<!-- Master page  -->
@extends('backend.layouts.master')
<!-- Page title -->
@section('pageTitle') Tableau de bord @endsection
<!-- End block -->
@section('extraStyle')
<link rel="stylesheet" href="{{URL::asset('leaflet/leaflet.css')}}">
<script src="{{URL::asset('leaflet/leaflet.js')}}"></script>
<style>
    .notification li {
        font-size: 16px;
    }
    .notification li.info span.badge {
        background: #00c0ef;
    }
    .notification li.warning span.badge {
        background: #f39c12;
    }
    .notification li.success span.badge {
        background: #00a65a;
    }
    .notification li.error span.badge {
        background: #dd4b39;
    }
    .total_bal {
        margin-top: 5px;
        margin-right: 5%;
    }
</style>
@endsection
<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box ">
                <a class="small-box-footer bg-orange-dark" href="#">
                    <div class="icon bg-orange-dark" style="padding: 9.5px 18px 8px 18px;">
                        <i class="fa icon-school"></i>
                    </div>
                    <div class="inner ">
                        <h3 class="text-white">{{$salles_tp_dispo}} </h3>
                        <p class="text-white">
                            Salles TP disponibles </p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <div class="small-box ">
                <a class="small-box-footer bg-pink-light" href="#">
                    <div class="icon bg-pink-light" style="padding: 9.5px 18px 8px 18px;">
                        <i class="fa icon-teacher"></i>
                    </div>
                    <div class="inner ">
                        <h3 class="text-white">
                            {{$salles_tp_amphi}} </h3>
                        <p class="text-white">
                        Amphithéâtres disponibles </p>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box ">
                <a class="small-box-footer bg-purple-light" href="#">
                    <div class="icon bg-purple-light" style="padding: 9.5px 18px 8px 18px;">
                        <i class="fa icon-fine"></i>
                    </div>
                    <div class="inner ">
                        <h3 class="text-white">
                            {{$salles_td_dispo}} </h3>
                        <p class="text-white">
                        Salles TD disponibles </p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
                <div class="col-lg-6 col-xs-12">
                    <div class="small-box ">
                        <a class="small-box-footer bg-teal-light" href="#">
                            <div class="icon bg-teal-light" style="padding: 5px 2px 2px 2px;">
                                <i class="fa fa-object-group"></i>
                            </div>
                            <div class="inner ">
                                <h3 class="text-white">
                                    {{$salles_autres}} </h3>
                                <p class="text-white">
                                Autres  </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="small-box ">
                        <a class="small-box-footer bg-teal-light" href="#">
                           
                            <div class="inner ">
                                <h3 class="text-white">
                                    {{$salles_total}} </h3>
                                <p class="text-white">
                                Total des salles </p>
                            </div>
                        </a>
                    </div>
                </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 ">
            <div class="col-md-9">
                <div style="height:45rem; width:auto" id="mapid"></div>
            </div>
            <div class="col-md-3">
                <h3 class="ds-map-filter" style="border-bottom: 1px solid #b6b6b6;">Détails de la salle</h3>
                <div id="info_salle_sideBar"> Veuillez cliquer sur un marquer afin d'avoir toutes les informations liées à la salle ... </div>
                <div id="info_salle_sideBar_avenir"> </div>
            </div>
        </div>
    </div>

<script>
window.onload = function () {
    console.log("Page chargée !");
    var mymap = L.map('mapid').setView([6.90766, -6.43975], 19);
    /* var tileStreets_ = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
     maxZoom: 18,
     id: 'mapbox.streets',
     accessToken: 'pk.eyJ1IjoibW9iaW9hbGV4IiwiYSI6ImNqeGc4OGgzNzB6dzMzeW13d3pzcGp5eXEifQ.gS8vSgW17EtggW0SeeXC3w'
     });*/
    var tileStreets = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 50,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });
    tileStreets.addTo(mymap);
    /*
     var marker1 = L.marker([6.90774, - 6.44036]).addTo(mymap);
     marker1.bindPopup("<b>Université JLG Université JLG Université JLG Université JLG</b><br>Salle RC - TP6").openPopup();
     var marker2 = L.marker([6.90767, - 6.44035]).addTo(mymap);
     marker2.bindPopup("<b>Université JLG</b><br>Salle RC - TP8").openPopup();
     var marker3 = L.marker([6.90778, - 6.44024]).addTo(mymap);
     marker3.bindPopup("<b>Université JLG</b><br>Salle Repro").openPopup();
     */
    //Get and Add salles positions

var LeafIcon = L.Icon.extend({
    options: {
        customId: "",
        iconSize: [10, 10],
        iconAnchor: [5, 10],
        popupAnchor: [-3, -26],
        zoomControl: false
    }
});

@if ($reservationList != null)
            console.log("Données à afficher :");
            var salleDatas = [];
            var salleDatas = {!! $reservationList !!};
            console.log(salleDatas);

            salleDatas.forEach(function(salle) {
                var  myIcon = new LeafIcon({iconUrl: 'images/MarquerLibre.png'});
                if(salle.date_debut == null){
                     myIcon = new LeafIcon({iconUrl: 'images/MarquerLibre.png'});
                    }
                    else{
                        myIcon = new LeafIcon({iconUrl: 'images/MarquerOccuper.png'});
                    }
                marker_salle = L.marker([salle.latitude.replace(',', '.'), salle.longitude.replace(',', '.')], {customId:salle.id, icon: myIcon}).addTo(mymap).on('click', onClickSurMarker);
                marker_salle.bindPopup("");
                //marker_salle.bindPopup("<b class='badge badge-primary'> salle.libelle </b><br> salle.description");
            });
           
 @endif


    window.postUrl = '{{URL::Route("map.salleinfo", 0)}}';
    let stopchange = false;
    function onClickSurMarker(e) {
        let that = $(this);
        var customId = this.options.customId;

        console.log(that);
        let pk = customId;
        let newpostUrl = postUrl.replace(/\.?0+$/, pk);

        axios.get(newpostUrl)
                .then((response) => {
                    if (response.data != null) {
                        //toastr.success(response.data.message);
                        var result = response.data;
                        var reservation_dates = "";
                        //Popup Infobulle
                        var designStatus = '<b class="label label-success"> LIBRE </b>';
                        var classStatus = "<span class='label label-success'>LIBRE</span>";
                        var html = '<div class="box box-success"><div class="box-body box-profile">';
                        if (result.reservation != null) {
                            reservation_dates = "<hr /> De " + result.reservation.date_debut + " à " + result.reservation.date_fin;
                            if (result.statutReservation == 'OCCUPEE') {
                                designStatus = '<b class="label label-danger">' + result.statutReservation + '</b>';
                                classStatus = "<span class='label label-danger'>OCCUPEE</span>";
                            }
                        }
                        var popup_content = "<span style='font-size:1.5rem;font-weight: bold;'>" + result.salle.libelle + "</span> " + classStatus + " <br>" + reservation_dates;
                        this._popup.setContent(popup_content);
                        html = '<div class="box box-info"><div class="box-body box-profile">';
                        html += designStatus;
                        html += '<h5 class="profile-username text-left">';
                        html += result.salle.libelle;
                        html += '</h5>';
                        html += '<p class="text-muted text-left">';
                        html += result.salle.description;
                        html += '</p>';
                        if (result.reservation != null) {
                            html += '<hr />';
                            if (result.activite != null) {
                                html += '<p class="text-muted text-left">';
                                html += '<strong>Type d\activité: </strong>' + result.activite;
                                if (result.coursEnseigne != null) {
                                    html += '<br><strong>Cours enseigné: </strong>' + result.coursEnseigne;
                                }
                                if (result.animateur != null) {
                                    html += '<br><strong>Animateur: </strong>' + result.animateur;
                                }
                                html += '</p>';
                            }

                            html += "<p style='color:black;' class='text-muted text-left'>";
                            html += '<strong>Début: </strong>' + result.reservation.date_debut + ' <br>';
                            html += '<strong>Fin: </strong>' + result.reservation.date_fin + ' <br>';
                             html += '<strong>Fin: </strong>' + result.dateDebut + ' <br>';
                            html += '</p>';
                            html += '</div> </div>';
                        }
                        $('#info_salle_sideBar').html(html);
                        //A venir 

                        if (result.reservationAVenir != null) {
                            var htmlAvenir = '<div class="box box-danger"><div class="box-body box-profile">';
                            var designStatusAvenir = '<b class="label label-danger"> A VENIR </b>';
                            htmlAvenir += designStatusAvenir;

                            if (result.activiteAVenir != null) {
                                htmlAvenir += '<p class="text-muted text-left">';
                                htmlAvenir += '<strong>Type d\activité: </strong>' + result.activiteAVenir;
                                if (result.coursEnseigneAVenir != null) {
                                    htmlAvenir += '<br><strong>Cours enseigné: </strong>' + result.coursEnseigneAVenir;
                                }
                                if (result.animateurAVenir != null) {
                                    htmlAvenir += '<br><strong>Animateur: </strong>' + result.animateurAVenir;
                                }
                                htmlAvenir += '</p>';
                            }

                            htmlAvenir += "<p style='color:black;' class='text-muted text-left'>";
                            htmlAvenir += '<strong>Début: </strong>' + result.reservationAVenir.date_debut + ' <br>';
                            htmlAvenir += '<strong>Fin: </strong>' + result.reservationAVenir.date_fin + ' <br>';
                            htmlAvenir += '</p>';
                            htmlAvenir += '</div> </div>';
                            $('#info_salle_sideBar_avenir').html(htmlAvenir);

                        } else {
                            $('#info_salle_sideBar_avenir').html('<b></b>');
                        }

                        console.log(result);

                    }
                }).catch((error) => {
            // console.log(error.response);
            console.log(error);

        });

    }

};
    </script>

</div>
</div>




</section>
<!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')


<script src="{{asset(mix('js/dashboard.js'))}}"></script>
<script type="text/javascript">

$(document).ready(function () {
    Dashboard.init();
});

</script>
@endsection
<!-- END PAGE JS-->
