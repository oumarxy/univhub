<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Reservation  @endsection
<!-- End block -->

<!-- Page body extra css -->
@section('extraStyle')
    <style>
        @media print {
            @page {
                size:  A4 landscape;
                margin: 5px;
            }
        }
    </style>
@endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
        Autre Programmation
            <small>Détails</small>
        </h1>
        <div class="btn-group">
            <a href="#"  class="btn-ta btn-sm-ta btn-print btnPrintInformation"><i class="fa fa-print"></i> Imprimer</a>
        </div>
        
        <div class="btn-group">
            <a href="{{URL::route('reservation.edit',$reservation->id)}}" class="btn-ta btn-sm-ta"><i class="fa fa-edit"></i> Editer</a>
        </div>

        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
            <li><a href="{{URL::route('reservation.index')}}"><i class="fa icon-reservation"></i> Reservation</a></li>
            <li class="active">Voir</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content main-contents">
        <div class="row">
            <div class="col-md-12">
                <div id="printableArea">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="box box-info">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="@if($reservation->photo ){{ asset('storage/employee')}}/{{ $reservation->photo }} @else {{ asset('images/idlogo.png')}} @endif">
                                    <h3 class="profile-username text-center">{{$reservation->salle->libelle}}</h3>
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Salle</b> <a class="pull-right">{{$reservation->salle->libelle}}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Animateur</b> <a class="pull-right">{{$reservation->animateur}}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Activité</b> <a class="pull-right">{{$reservation->activite['libelle'] }}</a>
                                        </li>
                                         <li class="list-group-item" style="background-color: #FFF">
                                            <b>Date début</b> <a class="pull-right">{{$reservation->date_debut }}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Date fin</b> <a class="pull-right">{{$reservation->date_fin }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btnPrintInformation').click(function () {
                $('ul.nav-tabs li:not(.active)').addClass('no-print');
                $('ul.nav-tabs li.active').removeClass('no-print');
                window.print();
            });
        });
    </script>
@endsection
<!-- END PAGE JS-->
