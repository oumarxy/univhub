<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Reservation @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
<!-- Section header -->
<section class="content-header">
    <h1>
        Autres programmation
        <small>@if($reservation) Modifier @else Ajouter @endif</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
        <li><a href="{{URL::route('reservationautre.index')}}"><i class="fa icon-teacher"></i>Autre programmation</a></li>
        <li class="active">@if($reservation) Modifier @else Ajouter @endif</li>
    </ol>
</section>
<!-- ./Section header -->
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <form novalidate id="entryForm" action="@if($reservation) {{URL::Route('reservationautre.update', $reservation->id)}} @else {{URL::Route('reservationautre.store')}} @endif" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        @csrf
                        @if($reservation)  {{ method_field('PATCH') }} @endif
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="salle">Salle<span class="text-danger">*</span>
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="selectionner une salle"></i>
                                    </label>
                                    {!! Form::select('salle_id', $salles, $salle_id , ['class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('salle_id') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="dob">Date début<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type='text' class="form-control" readonly id='datetimepicker_debut'  name="date_debut" placeholder="date time" value="@if($reservation){{ $reservation->date_debut }}@else{{old('date_debut')}}@endif" required minlength="15" maxlength="19" />
                                    <span class="fa fa-calendar form-control-feedback"></span>
                                </div>   
                            </div>   
                            <div class="col-md-4">
                                <label for="dob">Date fin<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type='text' class="form-control" id='datetimepicker_fin' readonly  name="date_fin" placeholder="date time" value="@if($reservation){{ $reservation->date_fin }}@else{{old('date_fin')}}@endif" required minlength="15" maxlength="19" />
                                    <span class="fa fa-calendar form-control-feedback"></span>
                                </div>   
                            </div>   


                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="class_id">Activité
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Ajouter le niveau"></i>
                                        <span class="text-danger">*</span>
                                    </label>
                                    {!! Form::select('activite_id', $activites, $activite_id , ['id' => 'activite_id', 'placeholder' => 'Choisir une activité...','class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('activite_id') }}</span>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="libelle">Libellé</label>
                                    <input type="text" class="form-control" name="libelle" placeholder="Libelle" value="@if($reservation){{ $reservation->libelle }}@else{{old('libelle')}}@endif"  maxlength="255">
                                    <span class="fa fa-info form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('libelle') }}</span>
                                </div>
                            </div>


                           <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="animateur">Animateur</label>
                                    <input type="text" class="form-control" name="animateur" placeholder="Animateur" value="@if($reservation){{ $reservation->animateur }}@else{{old('animateur')}}@endif"  maxlength="255">
                                    <span class="fa fa-info form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('animateur') }}</span>
                                </div>
                            </div>



                        </div>   
                        <hr>
                        <div class="row">
                           

                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" name="description" placeholder="Description" value="@if($reservation){{ $reservation->description }}@else{{old('description')}}@endif"  maxlength="255">
                                    <span class="fa fa-info form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>
                            </div>

                        </div>
                        <hr>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{URL::route('reservationautre.index')}}" class="btn btn-default">Annuler</a>
                        <button type="submit" class="btn btn-info pull-right"><i class="fa @if($reservation) fa-refresh @else fa-plus-circle @endif"></i> @if($reservation) Modifier @else Ajouter @endif</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
<script type="text/javascript">
    $(document).ready(function () {
        var dateNow = new Date();
        Generic.initCommonPageJS();
        $('#datetimepicker_debut').datetimepicker({
            format: "YYYY/MM/DD HH:mm:s",
            viewMode: 'days',
            ignoreReadonly: true,
            showTodayButton:true,
            showClose:true
        });

        $('#datetimepicker_fin').datetimepicker({
            format: "YYYY/MM/DD HH:mm:s",
            viewMode: 'days',
            ignoreReadonly: true,
            showTodayButton:true,
            showClose:true,
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker_debut").on("dp.change", function (e) {
            $('#datetimepicker_fin').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker_fin").on("dp.change", function (e) {
            $('#datetimepicker_debut').data("DateTimePicker").maxDate(e.date);
        });
        Init();
    });


    function Init() {

    }
    

</script>
@endsection
<!-- END PAGE JS-->