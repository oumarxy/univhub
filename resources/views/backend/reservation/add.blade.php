<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Reservation @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
<!-- Section header -->
<section class="content-header">
    <h1>
        Programmation
        <small>@if($reservation) Modifier @else Ajouter @endif</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
        <li><a href="{{URL::route('reservation.index')}}"><i class="fa icon-teacher"></i> Programmation</a></li>
        <li class="active">@if($reservation) Modifier @else Ajouter @endif</li>
    </ol>
</section>
<!-- ./Section header -->
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <form novalidate id="entryForm" action="@if($reservation) {{URL::Route('reservation.update', $reservation->id)}} @else {{URL::Route('reservation.store')}} @endif" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        @csrf
                        @if($reservation)  {{ method_field('PATCH') }} @endif
                        <div class="row">
                             @if($reservation)  
                             
                              <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="salle">Salle<span class="text-danger">*</span>
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="selectionner une salle"></i>
                                    </label>
                                    {!! Form::select('salle_id', $salles, $salle_id , ['class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('salle_id') }}</span>
                                </div>
                            </div>
                             @else
                                <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="salle">Salle<span class="text-danger">*</span>
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="selectionner des salles"></i>
                                    </label>
                                    {!! Form::select('salle_ids', $salles, null, ['class' => 'form-control select2 add-salles', 'required' => 'true','multiple'=>'multiple','name'=>'salle_ids[]']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('salle_ids') }}</span>
                                </div>
                               </div>
                             @endif
                           
                            <div class="col-md-4">
                                <label for="dob">Date début<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type='text' class="form-control" readonly id='datetimepicker_debut'  name="date_debut" placeholder="date time" value="@if($reservation){{ $reservation->date_debut }}@else{{old('date_debut')}}@endif" required minlength="15" maxlength="19" />
                                    <span class="fa fa-calendar form-control-feedback"></span>
                                </div>   
                            </div>   
                            <div class="col-md-4">
                                <label for="dob">Date fin<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type='text' class="form-control" id='datetimepicker_fin' readonly  name="date_fin" placeholder="date time" value="@if($reservation){{ $reservation->date_fin }}@else{{old('date_fin')}}@endif" required minlength="15" maxlength="19" />
                                    <span class="fa fa-calendar form-control-feedback"></span>
                                </div>   
                            </div>   


                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="class_id">Niveau
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Ajouter le niveau"></i>
                                        <span class="text-danger">*</span>
                                    </label>
                                    {!! Form::select('class_id', $classes, $class_id , ['id' => 'niveau_id', 'placeholder' => 'Ajouter le niveau...','class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('class_id') }}</span>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="section_id">Groupe
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Ajouter la classe"></i>
                                        <span class="text-danger">*</span>
                                    </label>
                                    {!! Form::select('section_id', $sections, $section_id , ['placeholder' => 'Ajouter le niveau...','class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('section_id') }}</span>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="subject_id">Enseignement
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Ajouter la classe"></i>
                                        <span class="text-danger">*</span>
                                    </label>
                                    {!! Form::select('subject_id', $subjects, $subject_id , ['placeholder' => 'Ajouter la matière...','class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('subject_id') }}</span>
                                </div>
                            </div>



                        </div>   
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="subject_id">Enseignant
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Ajouter la classe"></i>
                                        <span class="text-danger">*</span>
                                    </label>
                                    {!! Form::select('teacher_id', $teachers, $teacher_id , ['placeholder' => 'Ajouter un enseignant...','class' => 'form-control select2', 'required' => 'true']) !!}
                                    <span class="form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group has-feedback">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" name="description" placeholder="Description" value="@if($reservation){{ $reservation->description }}@else{{old('description')}}@endif"  maxlength="255">
                                    <span class="fa fa-info form-control-feedback"></span>
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                </div>
                            </div>

                        </div>
                        <hr>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{URL::route('reservation.index')}}" class="btn btn-default">Annuler</a>
                        <button type="submit" class="btn btn-info pull-right"><i class="fa @if($reservation) fa-refresh @else fa-plus-circle @endif"></i> @if($reservation) Modifier @else Ajouter @endif</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
<script type="text/javascript">
    $(document).ready(function () {
        $('.add-salles').select2();
        var dateNow = new Date();
        Generic.initCommonPageJS();
        window.section_list_url = '{{URL::Route("academic.section")}}';
        window.subject_list_url = '{{URL::Route("reservation.matiere")}}';
        $('#datetimepicker_debut').datetimepicker({
            format: "YYYY/MM/DD HH:mm:s",
            viewMode: 'days',
            ignoreReadonly: true,
            showTodayButton:true,
            showClose:true
        });

        $('#datetimepicker_fin').datetimepicker({
            format: "YYYY/MM/DD HH:mm:s",
            viewMode: 'days',
            ignoreReadonly: true,
            showTodayButton:true,
            showClose:true,
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker_debut").on("dp.change", function (e) {
            $('#datetimepicker_fin').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker_fin").on("dp.change", function (e) {
            $('#datetimepicker_debut').data("DateTimePicker").maxDate(e.date);
        });
        Init();
    });


    function Init() {

        $('select[name="class_id"]').on('change', function () {
            let class_id = $(this).val();
            getSection(class_id);

        });

        $('#niveau_id').on('change', function () {
            //get subject of requested class
            Generic.loaderStart();
            let class_id = $(this).val();
            getSubject(class_id, function (res = {}) {
                // console.log(res);
                if (Object.keys(res).length) {

                    $('select[name="subject_id"]').empty().prepend('<option selected=""></option>').select2({placeholder: 'Pick a subject...', data: res});

                } else {
                    // clear subject list dropdown
                    $('select[name="subject_id"]').empty().select2({placeholder: 'Pick a subject...'});
                    toastr.warning('This class have no subject!');
                }
                Generic.loaderStop();
            });

        });



    }
    function  getSection(class_id) {
        let getUrl = window.section_list_url + "?class=" + class_id;
        if (class_id) {
            Generic.loaderStart();
            axios.get(getUrl)
                    .then((response) => {
                        if (Object.keys(response.data).length) {
                            $('select[name="section_id"]').empty().prepend('<option selected=""></option>').select2({allowClear: true, placeholder: 'Pick a section...', data: response.data});
                        } else {
                            $('select[name="section_id"]').empty().select2({placeholder: 'Pick a section...'});
                            toastr.error('This class have no section!');
                        }
                        Generic.loaderStop();
                    }).catch((error) => {
                let status = error.response.statusText;
                toastr.error(status);
                Generic.loaderStop();

            });
        } else {
            // clear section list dropdown
            $('select[name="section_id"]').empty().select2({placeholder: 'Pick a section...'});
        }
    }
    function  getSubject(class_id, cb) {
        let getUrl = window.subject_list_url + "?class=" + class_id;
        if (class_id) {
            console.log(getUrl);
            axios.get(getUrl)
                    .then((response) => {
                        cb(response.data);

                    }).catch((error) => {
                let status = error.response.statusText;
                console.log(error);
                toastr.error(status);
                cb();

            });
        } else {
            cb();
        }
    }



</script>
@endsection
<!-- END PAGE JS-->