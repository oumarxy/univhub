<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Enseignant Profil @endsection
<!-- End block -->

<!-- Page body extra css -->
@section('extraStyle')
    <style>
        @media print {
            @page {
                size:  A4 landscape;
                margin: 5px;
            }
        }
    </style>
@endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <div class="btn-group">
            <a href="#"  class="btn-ta btn-sm-ta btn-print btnPrintInformation"><i class="fa fa-print"></i> Imprimer</a>
        </div>
        <div class="btn-group">
            <a  href="{{URL::route('teacher.show',$teacher->id)}}?print_idcard=1" class="btn-ta btn-sm-ta" target="_blank"><span class="fa fa-floppy-o"></span> ID Card </a>
        </div>
        <div class="btn-group">
            <a href="{{URL::route('teacher.edit',$teacher->id)}}" class="btn-ta btn-sm-ta"><i class="fa fa-edit"></i> Modifier</a>
        </div>

        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
            <li><a href="{{URL::route('teacher.index')}}"><i class="fa icon-teacher"></i> Enseignant</a></li>
            <li class="active">Aperçu</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content main-contents">
        <div class="row">
            <div class="col-md-12">
                <div id="printableArea">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="box box-info">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="@if($teacher->photo ){{ asset('storage/employee')}}/{{ $teacher->photo }} @else {{ asset('images/avatar.jpg')}} @endif">
                                    <h3 class="profile-username text-center">{{$teacher->name}}</h3>
                                    <p class="text-muted text-center">{{$teacher->designation}}</p>
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Num. CNI</b> <a class="pull-right">{{$teacher->id_card}}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Num. Téléphone</b> <a class="pull-right">{{$teacher->phone_no}}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Email</b> <a class="pull-right">{{$teacher->email}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                           
                        </div>

                        <div class="col-sm-9">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                                    <li ><a href="#classList" data-toggle="tab">Niveau-Spécialité/Classe</a></li>
                                    <li><a href="#subjects" data-toggle="tab">Matière (ECUE)</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Nom</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->name}}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Date de naissance</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->dob}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Diplome</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->qualification}}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Grade</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->designation}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Genre</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->gender}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Date d'intégration</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->joining_date}}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Nom d'utilisateur</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->user->username}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Adresse</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p for="">: {{$teacher->address}}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Signature</label>
                                            </div>
                                            <div class="col-md-3">
                                                @if($teacher->signature )
                                                    <img class="img-responsive" src="{{ asset('storage/employee/signature')}}/{{ $teacher->signature }}">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="classList">
                                        <table class="table table-responsive table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Niveau & Spécialité</th>
                                                <th class="text-center">Classe</th>
                                            </tr>
                                            <tbody>
                                            @foreach($sections as $section)
                                                <tr>
                                                    <td class="text-center">
                                                        {{$section->class->name}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{$section->name}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="subjects">
                                        <table class="table table-responsive table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Niveau</th>
                                                <th class="text-center">Enseignant</th>
                                            </tr>
                                            <tbody>
                                            @foreach($subjects as $subject)
                                                <tr>
                                                    <td class="text-center">
                                                        {{$subject->class->name}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{$subject->name}}[{{$subject->code}}]
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btnPrintInformation').click(function () {
                $('ul.nav-tabs li:not(.active)').addClass('no-print');
                $('ul.nav-tabs li.active').removeClass('no-print');
                window.print();
            });
        });
    </script>
@endsection
<!-- END PAGE JS-->
