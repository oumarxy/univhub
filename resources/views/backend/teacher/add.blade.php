<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Enseignant @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Enseignant
            <small>@if($teacher) Modifier @else Nouveau @endif</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
            <li><a href="{{URL::route('teacher.index')}}"><i class="fa icon-teacher"></i> Enseignant</a></li>
            <li class="active">@if($teacher) Modifier @else Nouveau @endif</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($teacher) {{URL::Route('teacher.update', $teacher->id)}} @else {{URL::Route('teacher.store')}} @endif" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            @csrf
                            @if($teacher)  {{ method_field('PATCH') }} @endif
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="name">Nom<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="name" placeholder="Nom" value="@if($teacher){{ $teacher->name }}@else{{old('name')}}@endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="prenom">Prénom<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="prenom" placeholder="Prénom" value="@if($teacher){{ $teacher->prenom }}@else{{old('prenom')}}@endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('prenom') }}</span>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                         <label for="designation">Grade
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Grade"></i>
                                        </label>
                                        {!! Form::select('designation', AppHelper::GRADEENSEIGNANT, $designation , ['class' => 'form-control select2', 'required' => 'false']) !!}
                                       <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('designation') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="qualification">Diplôme</label>
                                        <input type="text" class="form-control" name="qualification" placeholder="Diplôme" value="@if($teacher){{ $teacher->qualification }}@else{{old('qualification')}}@endif"  maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('qualification') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="dob">Date de naissance</label>
                                        <input type='text' class="form-control date_picker2"  readonly name="dob" placeholder="date" value="@if($teacher){{ $teacher->dob }}@else{{old('dob')}}@endif" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('dob') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="gender">Genre<span class="text-danger">*</span>
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Selectionner le genre"></i>
                                        </label>
                                        {!! Form::select('gender', AppHelper::GENDER, $gender , ['class' => 'form-control select2', 'required' => 'true']) !!}
                                        <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="id_card">Num. CNI</label>
                                        <input  type="text" class="form-control" name="id_card"  placeholder="id card number" value="@if($teacher){{$teacher->id_card}}@else{{old('id_card')}}@endif" required minlength="4" maxlength="50">
                                        <span class="fa fa-id-card form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('id_card') }}</span>
                                    </div>
                                </div>
                           
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="email">Email<span class="text-danger">*</span></label>
                                        <input  type="email" class="form-control" name="email"  placeholder="email address" value="@if($teacher){{$teacher->email}}@else{{old('email')}}@endif" maxlength="100" required>
                                        <span class="fa fa-envelope form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="phone_no">Num. téléphone<span class="text-danger">*</span></label>
                                        <input  type="text" class="form-control" name="phone_no" required placeholder="phone or mobile number" value="@if($teacher){{$teacher->phone_no}}@else{{old('phone_no')}}@endif" min="8" maxlength="15">
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="joining_date">Date d'intégration<span class="text-danger">*</span></label>
                                        <input type='text' class="form-control date_picker2"  readonly name="joining_date" placeholder="date" value="@if($teacher){{$teacher->joining_date}}@else{{ old('joining_date') }}@endif" required minlength="10" maxlength="255" />
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('joining_date') }}</span>
                                    </div>
                                </div>
                           
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="nationalite">Nationalité</label>
                                        <input  type="text" class="form-control" name="nationalite"  placeholder="nationalité" value="@if($teacher){{$teacher->nationalite}}@else{{old('nationalite')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('nationalite') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="fonction_administrative">Fonction administrative</label>
                                        <input  type="text" class="form-control" name="fonction_administrative" required placeholder="Fonction administrative" value="@if($teacher){{$teacher->fonction_administrative}}@else{{old('fonction_administrative')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('fonction_administrative') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="situation_matrimoniale">Situation matrimoniale</label>
                                        <input  type="text" class="form-control" name="situation_matrimoniale" required placeholder="Fonction administrative" value="@if($teacher){{$teacher->situation_matrimoniale}}@else{{old('situation_matrimoniale')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('situation_matrimoniale') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="signature">Departement</label>
                                        <input  type="number" class="form-control" name="signature"  placeholder="Departement" value="@if($teacher){{$teacher->signature}}@else{{old('signature')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('signature') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="username">Spécialité<span class="text-danger">*</span></label>
                                        <input  type="text" class="form-control" name="specialite" required placeholder="Fonction administrative" value="@if($teacher){{$teacher->specialite}}@else{{old('specialite')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('specialite') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="ufr">UFR</label>
                                        <input  type="text" class="form-control" name="ufr" required placeholder="Fonction administrative" value="@if($teacher){{$teacher->ufr}}@else{{old('ufr')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('ufr') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                         <label for="type_enseignant">Type d'enseignant
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="type enseignant ?"></i>
                                        </label>
                                        {!! Form::select('type_enseignant', AppHelper::TYPEENSEIGNANT, $type_enseignant , ['class' => 'form-control select2', 'required' => 'false']) !!}
                                       <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('type_enseignant') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                         <label for="permissionnaire">Permissionnaire
                                            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Est Permissionnaire ?"></i>
                                        </label>
                                        {!! Form::select('permissionnaire', AppHelper::PERMISSIONNAIRE, $permissionnaire , ['class' => 'form-control select2', 'required' => 'false']) !!}
                                       <span class="form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('permissionnaire') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label for="permission_start">Début permission</label>
                                        <input  type="text" class="form-control date_picker2" readonly name="permission_start"  placeholder="Date" value="@if($teacher){{$teacher->permission_start}}@else{{old('permission_start')}}@endif" minlength="10" maxlength="255">
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('permission_start') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group has-feedback">
                                        <label for="permission_end">Fin permission</label>
                                        <input  type="text" class="form-control date_picker2" readonly name="permission_end"  placeholder="Date" value="@if($teacher){{$teacher->permission_end}}@else{{old('permission_end')}}@endif" minlength="10" maxlength="255">
                                        <span class="fa fa-calendar form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('permission_end') }}</span>
                                    </div>
                                </div>
                               
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="address">Adresse</label>
                                        <textarea name="address" class="form-control"  maxlength="500" >@if($teacher){{ $teacher->address }}@else{{ old('address') }} @endif</textarea>
                                        <span class="fa fa-location-arrow form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('address') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="photo">Photo<br><span class="text-danger">[Taille min 150 X 150 et max 200kb]</span></label>
                                        <input  type="file" class="form-control" accept=".jpeg, .jpg, .png" name="photo" placeholder="Photo image">
                                        @if($teacher && isset($teacher->photo))
                                            <input type="hidden" name="oldPhoto" value="{{$teacher->photo}}">
                                        @endif
                                        <span class="glyphicon glyphicon-open-file form-control-feedback" style="top:45px;"></span>
                                        <span class="text-danger">{{ $errors->first('photo') }}</span>
                                    </div>
                                </div>

                            </div>

                            <hr>
                            <div class="row">
                                @if(!$teacher)
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="username">Nom d'utilisateur<span class="text-danger">*</span></label>
                                        <input  type="text" class="form-control" value="" name="username" required minlength="5" maxlength="255">
                                        <span class="glyphicon glyphicon-info-sign form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('username') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="password">Mot de passe<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" name="password" placeholder="Password" required minlength="6" maxlength="50">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    </div>
                                </div>
                                @endif
                                @if($teacher && !$teacher->user_id)
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <label for="user_id">Utilisateur
                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Leave as it is, if not need user"></i>
                                            </label>
                                            {!! Form::select('user_id', $users, null , ['placeholder' => 'Pick if needed','class' => 'form-control select2']) !!}
                                            <span class="form-control-feedback"></span>
                                            <span class="text-danger">{{ $errors->first('user_id') }}</span>
                                        </div>
                                    </div>

                                @endif
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{URL::route('teacher.index')}}" class="btn btn-default">Annuler</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa @if($teacher) fa-refresh @else fa-plus-circle @endif"></i> @if($teacher) Mise à jour @else Ajouter @endif</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Generic.initCommonPageJS();
        });
    </script>
@endsection
<!-- END PAGE JS-->
