<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Salle @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Salle
            <small>@if($salle) Modifier @else Ajouter @endif</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
            <li><a href="{{URL::route('salle.index')}}"><i class="fa icon-teacher"></i> Salle</a></li>
            <li class="active">@if($salle) Modifier @else Ajouter @endif</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($salle) {{URL::Route('salle.update', $salle->id)}} @else {{URL::Route('salle.store')}} @endif" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            @csrf
                            @if($salle)  {{ method_field('PATCH') }} @endif
                            <div class="row">

                                 <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="code_salle">Catégorie<span class="text-danger">*</span></label>
                                        <select class="form-control" name="categorie" id="categorie" value="@if($salle){{ $salle->categorie }}@else{{old('categorie')}}@endif" required>
                                            <option>Choisir une catégorie</option>
                                            <option value="1" @if($salle && $salle->categorie == 1) selected @endif >Salle TP</option>
                                            <option value="2" @if($salle && $salle->categorie == 2) selected @endif >Amphithéâtre</option>
                                            <option value="3" @if($salle && $salle->categorie == 3) selected @endif >Salle TD</option>
                                            <option value="4" @if($salle && $salle->categorie == 4) selected @endif >Autre salle</option>
                                        </select>
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('categorie') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="code_salle">Code salle<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="code_salle" placeholder="libellé" value="@if($salle){{ $salle->code_salle }}@else{{old('code_salle')}}@endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('code_salle') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="libelle">Libellé<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="libelle" placeholder="libellé" value="@if($salle){{ $salle->libelle }}@else{{old('libelle')}}@endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('libelle') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="description">Description</label>
                                        <input type="text" class="form-control" name="description" placeholder="Description" value="@if($salle){{ $salle->description }}@else{{old('description')}}@endif"  maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                    </div>
                                </div>
                                
                           
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="longitude">Longitude</label>
                                        <input type="text" class="form-control" name="longitude" placeholder="Longitude" value="@if($salle){{ $salle->longitude }}@else{{old('longitude')}}@endif" >
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('longitude') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="latitude">Latitude</label>
                                        <input  type="text" class="form-control" name="latitude"  placeholder="Latitude" value="@if($salle){{$salle->latitude}}@else{{old('latitude')}}@endif" >
                                        <span class="fa fa-phone form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('latitude') }}</span>
                                    </div>
                                </div>

                            </div>

                            <hr>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{URL::route('salle.index')}}" class="btn btn-default">Annuler</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa @if($salle) fa-refresh @else fa-plus-circle @endif"></i> @if($salle) Modifier @else Ajouter @endif</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Generic.initCommonPageJS();
        });
    </script>
@endsection
<!-- END PAGE JS-->
