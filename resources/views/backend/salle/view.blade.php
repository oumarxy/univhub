<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Salle  @endsection
<!-- End block -->

<!-- Page body extra css -->
@section('extraStyle')
    <style>
        @media print {
            @page {
                size:  A4 landscape;
                margin: 5px;
            }
        }
    </style>
@endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <div class="btn-group">
            <a href="#"  class="btn-ta btn-sm-ta btn-print btnPrintInformation"><i class="fa fa-print"></i> Imprimer</a>
        </div>
        
        <div class="btn-group">
            <a href="{{URL::route('salle.edit',$salle->id)}}" class="btn-ta btn-sm-ta"><i class="fa fa-edit"></i> Editer</a>
        </div>

        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
            <li><a href="{{URL::route('salle.index')}}"><i class="fa icon-salle"></i> Salle</a></li>
            <li class="active">Voir</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content main-contents">
        <div class="row">
            <div class="col-md-12">
                <div id="printableArea">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="box box-info">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="@if($salle->photo ){{ asset('storage/employee')}}/{{ $salle->photo }} @else {{ asset('images/idlogo.png')}} @endif">
                                    <h3 class="profile-username text-center">{{$salle->libelle}}</h3>
                                    <p class="text-muted text-center">{{$salle->description}}</p>
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Catégorie</b> 
                                            @if($salle && $salle->categorie == 1)
                                              <a class="pull-right"> Salles TP </a>
                                            @elseif($salle && $salle->categorie == 2)
                                              <a class="pull-right"> Amphithéâtre </a>
                                            @elseif($salle && $salle->categorie == 3)
                                              <a class="pull-right"> Salles TD </a>
                                            @else
                                              <a class="pull-right"> Autre salle </a>
                                            @endif

                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Longitude</b> <a class="pull-right">{{$salle->longitude}}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Latitude</b> <a class="pull-right">{{$salle->latitude}}</a>
                                        </li>
                                        <li class="list-group-item" style="background-color: #FFF">
                                            <b>Statut</b> <a class="pull-right">{{$salle->status}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btnPrintInformation').click(function () {
                $('ul.nav-tabs li:not(.active)').addClass('no-print');
                $('ul.nav-tabs li.active').removeClass('no-print');
                window.print();
            });
        });
    </script>
@endsection
<!-- END PAGE JS-->
