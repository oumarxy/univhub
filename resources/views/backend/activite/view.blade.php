<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Activite  @endsection
<!-- End block -->

<!-- Page body extra css -->
@section('extraStyle')
<style>
    @media print {
        @page {
            size:  A4 landscape;
            margin: 5px;
        }
    }
</style>
@endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
<!-- Section header -->
<section class="content-header">
    <div class="btn-group">
        <a href="#"  class="btn-ta btn-sm-ta btn-print btnPrintInformation"><i class="fa fa-print"></i> Imprimer</a>
    </div>

    <div class="btn-group">
        <a href="{{URL::route('activite.edit',$activite->id)}}" class="btn-ta btn-sm-ta"><i class="fa fa-edit"></i> Editer</a>
    </div>

    <ol class="breadcrumb">
        <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
        <li><a href="{{URL::route('activite.index')}}"><i class="fa icon-activite"></i> Activite</a></li>
        <li class="active">Voir</li>
    </ol>
</section>
<!-- ./Section header -->
<!-- Main content -->
<section class="content main-contents">
    <div class="row">
        <div class="col-md-12">
            <div id="printableArea">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="box box-info">
                            <div class="box-body box-profile">
                                <h3 class="profile-username text-center">{{$activite->libelle}}</h3>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item" style="background-color: #FFF">
                                        <b>Activite</b> <a class="pull-right">{{$activite->libelle}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
<script type="text/javascript">
    $(document).ready(function () {
        $('.btnPrintInformation').click(function () {
            $('ul.nav-tabs li:not(.active)').addClass('no-print');
            $('ul.nav-tabs li.active').removeClass('no-print');
            window.print();
        });
    });
</script>
@endsection
<!-- END PAGE JS-->
