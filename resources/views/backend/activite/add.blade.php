<!-- Master page  -->
@extends('backend.layouts.master')

<!-- Page title -->
@section('pageTitle') Teacher @endsection
<!-- End block -->

<!-- Page body extra class -->
@section('bodyCssClass') @endsection
<!-- End block -->

<!-- BEGIN PAGE CONTENT-->
@section('pageContent')
    <!-- Section header -->
    <section class="content-header">
        <h1>
            Activité
            <small>@if($activite) Modifier @else Ajouter @endif</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{URL::route('user.dashboard')}}"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
            <li><a href="{{URL::route('activite.index')}}"><i class="fa icon-teacher"></i> Activite</a></li>
            <li class="active">@if($activite) Modifier @else Ajouter @endif</li>
        </ol>
    </section>
    <!-- ./Section header -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <form novalidate id="entryForm" action="@if($activite) {{URL::Route('activite.update', $activite->id)}} @else {{URL::Route('activite.store')}} @endif" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            @csrf
                            @if($activite)  {{ method_field('PATCH') }} @endif
                            <div class="row">
                               
                                <div class="col-md-4">
                                    <div class="form-group has-feedback">
                                        <label for="libelle">Libellé<span class="text-danger">*</span></label>
                                        <input autofocus type="text" class="form-control" name="libelle" placeholder="libellé" value="@if($activite){{ $activite->libelle }}@else{{old('libelle')}}@endif" required minlength="2" maxlength="255">
                                        <span class="fa fa-info form-control-feedback"></span>
                                        <span class="text-danger">{{ $errors->first('libelle') }}</span>
                                    </div>
                                </div>
                                
                               </div>

                           <hr>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="{{URL::route('activite.index')}}" class="btn btn-default">Annuler</a>
                            <button type="submit" class="btn btn-info pull-right"><i class="fa @if($activite) fa-refresh @else fa-plus-circle @endif"></i> @if($activite) Modifier @else Ajouter @endif</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
<!-- END PAGE CONTENT-->

<!-- BEGIN PAGE JS-->
@section('extraScript')
    <script type="text/javascript">
        $(document).ready(function () {
            Generic.initCommonPageJS();
        });
    </script>
@endsection
<!-- END PAGE JS-->
