<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during website frontend
    */

    'menu_home' => 'Acceuil',
    'menu_class' => 'Classe',
    'menu_teachers' => 'Enseignants',
    'menu_events' => 'Événements',
    'menu_gallery' => 'Galerie',
    'menu_contact_us' => 'Contactez nous',
    'menu_faq' => 'FAQ',
    'menu_admission' => 'Admission',
    'menu_result' => 'Résultats',
    'menu_timeline' => 'Timeline',
    'empty_content' => 'S\'il vous plaît ajouter du contenu à partir du panneau d\'administration।',
    'apply_now' => 'Appliquer maintenant',
    'about_us' => 'À propos de nous',
    'why_we' => 'Pourquoi nous sommes meilleurs',
    'about_us_more' => 'En savoir plus sur nous à partir de la vidéo',
    'about_us_more2' => 'Regarder plus',
    'service' => 'Nos services',
    'stat_students' => 'Etudiants inscrits',
    'stat_teachers' => 'Enseignants',
    'stat_college' => 'Passer à l\'université',
    'stat_books' => 'Livres',
    'testimonials' => 'Témoignages',
    'get_in' => 'Prenez contact avec nous',
    'drop_email' => 'Laissez votre email ici pour obtenir les dernières mises à jour de notre part.',
    'write_email' => 'Entrer votre Email',
    'subscribe' => 'Souscrire',
    'up_event' => 'Événement à venir',
    'help_links' => 'Liens d\'aide',
    'copy_right' => date('Y').' . Tous les droits sont réservés',
    'maintainer' => 'Entretenu par',
    'details' => 'Détails',
    'information' => 'Information',
    'teacher' => 'Professeur',
    'room_no' => 'Room No',
    'capacity' => 'Capacité',
    'shift' => 'Décalage',
    'description' => 'La description',
    'course_outline' => 'Plan de cours',
    'meet_teacher' => 'Rencontrez nos professeurs',
    'more' => 'More',
    'gallery_title' => 'Nos souvenirs',
    'contact_us_form_title' => 'Contactez nous',
    'yn' => 'Votre nom',
    'ye' => 'Votre email',
    'subject' => 'soumettre à',
    'ym' => 'Votre message',
    'send' => 'Envoyer',
    'address' => 'Adresse',
    'phone_no' => 'Numéro de téléphone',
    'email' => 'E-mail',
    'our_office' => 'Notre bureau',
    'faq_title' => 'Questions fréquemment posées',
    'timeline_title' => 'Notre voyage',
    'not_found' => '404',
    'not_found_title' => 'Cette page n\'existe pas.',
    'code_500' => '500',
    'code_500_title' => 'Erreur Interne du Serveur! Contacter l\'administrateur système.',



];
